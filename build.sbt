lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.seek.ben",
      scalaVersion := "2.12.8"
    )),
    name := "seek-test"
  )

enablePlugins(JavaAppPackaging)

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
