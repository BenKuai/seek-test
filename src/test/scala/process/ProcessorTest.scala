package process

import java.time.{LocalDate, LocalDateTime}

import com.seek.ben.domain.CarRecord
import com.seek.ben.processor._
import org.scalatest.FunSuite

class ProcessorTest extends FunSuite {

  test("should load file and return correct result (happy day)") {
    // given input file test.txt
    // when call process
    val result = IOProcessor.process(getClass.getResource("/test.txt").getPath)

    // then expect result
    val top = result.topRecords.toIndexedSeq
    val dayRecords = result.recordsPerDay
    assert(result.total == 159)

    assert(top.size == 3)
    assert(top.head == CarRecord(LocalDateTime.parse("2016-12-01T07:30:00"), 46))
    assert(top(1) == CarRecord(LocalDateTime.parse("2016-12-01T08:00:00"), 42))
    assert(top.last == CarRecord(LocalDateTime.parse("2016-12-01T07:00:00"), 25))

    assert(dayRecords.size == 1)
    assert(dayRecords.get(LocalDate.parse("2016-12-01")).isDefined)
    assert(dayRecords.get(LocalDate.parse("2016-12-01")).get == 159)

    assert(result.leastPeriodRecord.carCount == CarRecord(LocalDateTime.parse("2016-12-01T05:00:00"), 31))
  }

  test("should still return top record but no leas record if not enough data") {
    // given input iterator
    val input = Seq("2016-12-01T07:30:00   46", "2016-12-01T08:00:00 90").toIterator
    // when call process
    val result = LogicProcessor.process(input)

    // then expect result
    val top = result.topRecords
    val dayRecords = result.recordsPerDay
    assert(result.total == 136)

    assert(top.size == 2)
    assert(top.head == CarRecord(LocalDateTime.parse("2016-12-01T08:00:00"), 90))
    assert(top.last == CarRecord(LocalDateTime.parse("2016-12-01T07:30:00"), 46))

    assert(dayRecords.size == 1)
    assert(dayRecords.get(LocalDate.parse("2016-12-01")).isDefined)
    assert(dayRecords.get(LocalDate.parse("2016-12-01")).get == 136)

    assert(result.leastPeriodRecord.carCount == CarRecord.empty)
  }

  test("should still return top record but ignore not contiguous record for leas record calculation") {
    // given input iterator
    val input = Seq(
      "2016-12-01T07:30:00   100",
      "2016-12-01T08:30:00   40",
      "2016-12-01T09:00:00   50",
      "2016-12-01T09:30:00   60",
      "2016-12-01T10:30:00   4",
      "2016-12-01T11:00:00   6",
      "2016-12-01T11:30:00   8",
      "2016-12-02T08:30:00   46",
      "2016-12-02T09:00:00 90").toIterator
    // when call process
    val result = LogicProcessor.process(input)

    // then expect result
    val top = result.topRecords.toIndexedSeq
    val dayRecords = result.recordsPerDay
    assert(result.total == 404)

    assert(top.size == 3)
    assert(top.head == CarRecord(LocalDateTime.parse("2016-12-01T07:30:00"), 100))
    assert(top(1) == CarRecord(LocalDateTime.parse("2016-12-02T09:00:00"), 90))
    assert(top.last == CarRecord(LocalDateTime.parse("2016-12-01T09:30:00"), 60))

    assert(dayRecords.size == 2)
    assert(dayRecords.get(LocalDate.parse("2016-12-01")).isDefined)
    assert(dayRecords.get(LocalDate.parse("2016-12-01")).get == 268)
    assert(dayRecords.get(LocalDate.parse("2016-12-02")).isDefined)
    assert(dayRecords.get(LocalDate.parse("2016-12-02")).get == 136)

    assert(result.leastPeriodRecord.carCount ==
      CarRecord(LocalDateTime.parse("2016-12-01T10:30:00"), 18))
  }
}
