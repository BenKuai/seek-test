package com.seek.ben.processor

import com.seek.ben.domain.{CarRecord, LeastPeriodRecord}

import scala.collection.immutable.Queue

object LeastPeriodRecordProcessor {
  val PERIOD_RECORD_NUMBER = 3 // 3 records per 1.5 hour period
  val PERIOD_GAP_IN_MINUTES = 30 // half hour gap

  /*
  * Add new record to previous queue
  * 1) if queue size is greater than `PERIOD_RECORD_NUMBER`, remove the oldest record from the queue
  *    return the new queue with calculated new least record after comparing with previous one
  * 2) if queue size equals `PERIOD_RECORD_NUMBER`,
  *    return the new queue with calculated new least record after comparing with previous one
  * 3) if queue size is less than `PERIOD_RECORD_NUMBER`, not enough records for the period
  *    return current queue with previous least record
   */
  def processLeastPeriodRecord(previousLeast: LeastPeriodRecord, current: CarRecord,
                               periodSize: Int = PERIOD_RECORD_NUMBER): LeastPeriodRecord = {
    val previousLeastResult = previousLeast.carCount
    val currentQueue = enqueueRecord(previousLeast.previousItems, current)

    currentQueue.size match {
      case s if s > periodSize =>
        val newQueue = currentQueue.dequeue._2
        calculateNewLeastRecord(previousLeastResult, newQueue)

      case s if s == periodSize =>
        calculateNewLeastRecord(previousLeastResult, currentQueue)

      case _ =>
        LeastPeriodRecord(previousLeastResult, currentQueue)
    }

  }

  // it says "you can assume clean input" in the instruction
  // but the example data on page 2 is not contiguous every half hour
  // this method is to discard not contiguous records
  // assumption here is to only do the calculation if 3 contiguous records happen
  private def enqueueRecord(queue: Queue[CarRecord], record: CarRecord, gap: Int = PERIOD_GAP_IN_MINUTES) = {

    if (queue.isEmpty) queue.enqueue(record)
    else {
      val last = queue.last.time
      // only add item to the queue if it follows the last record in half hour
      // otherwise build new queue with current record
      if (last.plusMinutes(gap) == record.time)
        queue.enqueue(record)
      else Queue[CarRecord](record)
    }

  }

  private def calculateNewLeastRecord(previousLeastResult: CarRecord, newQueue: Queue[CarRecord]) = {
    val total = newQueue.map(_.count).sum
    val startTime = newQueue.head.time
    val newPeriodRecord = CarRecord(startTime, total)
    val newLeastResult =
      if (previousLeastResult == CarRecord.empty) newPeriodRecord
      else CarRecord.lessOf(previousLeastResult, newPeriodRecord)
    LeastPeriodRecord(newLeastResult, newQueue)
  }


}
