package com.seek.ben.processor

import java.time.LocalDate

import com.seek.ben.domain.{CarRecord, Result}

import scala.collection.SortedSet

object LogicProcessor {

  val TOP_RECORD_SIZE = 3

  def process(lines: Iterator[String]): Result = {
    // transform string to our domain then process record one by one
    lines.map(CarRecord.from).foldLeft(Result.empty)(processResult)
  }

  // calculate result with total, top records and least period record
  // given simple logic of the problem no dependency injection is used nor high level abstraction
  private def processResult(previousResult: Result, currentRecord: CarRecord): Result = {
    val total = previousResult.total + currentRecord.count
    val recordsPerDay = processRecordsPerDay(previousResult.recordsPerDay, currentRecord)
    val topRecords = processTopRecords(previousResult.topRecords, currentRecord)
    val leastPeriodRecord =
      LeastPeriodRecordProcessor.processLeastPeriodRecord(previousResult.leastPeriodRecord, currentRecord)
    Result(total, recordsPerDay, topRecords, leastPeriodRecord)
  }

  // by default keep only top 3 records
  // use build-in sorted set for sorting and `AscCarCountOrdering` is used by `Result.empty`
  private def processTopRecords(previousTopRecords: SortedSet[CarRecord], current: CarRecord,
                                topSize: Int = TOP_RECORD_SIZE): SortedSet[CarRecord] = {
    val newTopRecords = previousTopRecords + current
    val size = newTopRecords.size
    if (size > topSize) newTopRecords.dropRight(size - topSize)
    else newTopRecords
  }

  private def processRecordsPerDay(perviousResult: Map[LocalDate, Int], record: CarRecord) = {
    val recordDate = record.time.toLocalDate
    val value = perviousResult.get(recordDate)
    if (value.isDefined)
      perviousResult.updated(recordDate, value.get + record.count)
    else
      perviousResult + (recordDate -> record.count)

  }

}
