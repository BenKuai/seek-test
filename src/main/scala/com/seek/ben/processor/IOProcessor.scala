package com.seek.ben.processor

import com.seek.ben.domain.Result

import scala.io.Source

object IOProcessor {

  // assume that the task is to read only one file
  // given simple logic of the problem no dependency injection is used nor high level abstraction
  def process(filePath: String): Result = {
    val source = Source.fromFile(filePath)
    val result = LogicProcessor.process(source.getLines)
    sys.addShutdownHook(source.close())
    result
  }
}
