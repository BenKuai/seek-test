package com.seek.ben.main

import com.seek.ben.processor.IOProcessor

// main entry expects full path of the input file
object Main {
  def main(args: Array[String]): Unit = {
    // assume that file exists
    args match {
      case Array(filePath) =>
        val result = IOProcessor.process(filePath)
        println(result)
      case _ =>
        println("Usage: [Full path of input file.]")
    }
  }
}
