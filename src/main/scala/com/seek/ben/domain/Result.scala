package com.seek.ben.domain

import java.time.LocalDate

import scala.collection.SortedSet
import scala.util.Properties

// result includes total car counts, top car count and least period count
// assume integer is enough for this task as total, change to long otherwise
case class Result(total: Int,
                  recordsPerDay: Map[LocalDate, Int],
                  topRecords: SortedSet[CarRecord],
                  leastPeriodRecord: LeastPeriodRecord) {
  // pretty print
  override def toString: String = {
    val topInString =
      s"Top records:${Properties.lineSeparator}" +
        s"${topRecords.mkString(Properties.lineSeparator)}${Properties.lineSeparator}"

    val recordsPerDayString =
      s"Records Per Day:${Properties.lineSeparator}" +
        s"${recordsPerDay.map(p => s"${p._1} ${p._2}").mkString(Properties.lineSeparator)}" +
        Properties.lineSeparator

    s"Total [$total]${Properties.lineSeparator}" +
      recordsPerDayString + topInString +
      s"Least Period Record [$leastPeriodRecord]"

  }
}

object Result {
  // descending ordering for top records
  def empty =
    Result(0,
      Map[LocalDate, Int](),
      SortedSet[CarRecord]()(DesCarCountOrdering),
      LeastPeriodRecord.empty)
}
