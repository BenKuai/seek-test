package com.seek.ben.domain

import java.time.LocalDateTime

// base domain class with a time and count per record
case class CarRecord(time: LocalDateTime, count: Int) {
  // slightly different with input, the default time string has no seconds
  override def toString: String = s"${time.toString} $count"
}

object CarRecord {
  // assume clean input and space separated time and count each row
  def from(s: String) = {
    // partial function used
    // assume clean input that time is always in ISO format and count is a integer
    s.split("\\s+") match {
      case Array(t, c) =>
        CarRecord(LocalDateTime.parse(t.trim), c.trim.toInt)
    }
  }

  def empty = CarRecord(LocalDateTime.MIN, 0)

  // the less of two records for least period count
  // use ascending ordering
  def lessOf(c1: CarRecord, c2: CarRecord): CarRecord = {
    val result = AscCarCountOrdering.compare(c1, c2)
    result match {
      case -1 => c1
      case 1 => c2
      case 0 => c1
    }
  }

}

// ascending order for least count record
// if car count is the same use time order to compare records
// used for least period count record
object AscCarCountOrdering extends Ordering[CarRecord] {
  override def compare(x: CarRecord, y: CarRecord): Int = {
    (x, y) match {
      case (a, b) if a.count < b.count => -1
      case (a, b) if a.count > b.count => 1
      case (a, b) if a.time.isBefore(b.time) => -1
      case (a, b) if a.time.isAfter(b.time) => 1
      case _ => 0
    }
  }
}

// descending order for top count record
// used for top N count record
object DesCarCountOrdering extends Ordering[CarRecord] {
  override def compare(x: CarRecord, y: CarRecord): Int =
    -1 * AscCarCountOrdering.compare(x, y)

}
