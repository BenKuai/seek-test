package com.seek.ben.domain

import scala.collection.immutable.Queue

// car count time is the start time of 1.5 hour period and count is the total number during this period
// previous record in queue to cache car counts for calculation of 1.5 hour period car counts
case class LeastPeriodRecord(carCount: CarRecord, previousItems: Queue[CarRecord]) {
  override def toString: String =
    if (carCount == CarRecord.empty) "not found any least period record"
    else carCount.toString
}

object LeastPeriodRecord {
  def empty = LeastPeriodRecord(CarRecord.empty, Queue[CarRecord]())
}